from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from apps.cliente.views import index, cliente_view , cliente_list , cliente_edit , ClienteList , ClienteCreate , ClienteUpdate , ClienteDelete

urlpatterns = [
    url(r'^$', index,name='index'),
    url(r'^nuevo$',ClienteCreate.as_view(),name='cliente_crear'),
    url(r'^listar$',ClienteList.as_view(),name='cliente_listar'),
    url(r'^editar/(?P<pk>\d+)$',ClienteUpdate.as_view(),name='cliente_editar'),
    url(r'^eliminar/(?P<pk>\d+)$',ClienteDelete.as_view(),name='cliente_eliminar'),


]
