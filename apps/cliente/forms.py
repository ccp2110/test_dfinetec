from django import forms

from apps.cliente.models import Cliente

class ClienteForm(forms.ModelForm):

     class Meta:
         model = Cliente

         fields = [
             'nombre',
             'apellido',
             'ruc',
             'direccion',
             'telefono',
         ]
         labels ={
             'nombre' : 'Nombre',
             'apellido':'Apellido',
             'ruc':'RUC',
             'direccion':'Direccion',
             'telefono':'Telefono',
         }
         widgets={
             'nombre':forms.TextInput(attrs={'class':'form-control','size':'100'}),
             'apellido':forms.TextInput(attrs={'class':'form-control','size':'100'}),
             'ruc':forms.TextInput(attrs={'class':'form-control','size':'100'}),
             'direccion':forms.TextInput(attrs={'class':'form-control','size':'100'}),
             'telefono':forms.TextInput(attrs={'class':'form-control','size':'100'}),
         }