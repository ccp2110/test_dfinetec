from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView , CreateView , UpdateView , DetailView
from apps.venta.models import Venta
from apps.venta.forms import VentaForm
from django.urls import reverse_lazy
import datetime

class VentaList(ListView):
    model = Venta
    template_name = 'venta/venta_list.html'

class VentaCreate(CreateView):
    model=Venta
    form_class = VentaForm
    template_name = 'venta/venta_form.html'
    success_url = reverse_lazy('venta_listar')

    def form_valid(self, form):

        venta = form.save(commit=False)
        venta.producto.cant_vendida = venta.producto.cant_vendida + 1
        venta.producto.stock = venta.producto.stock - 1
        venta.fecha = datetime.datetime.now()
        venta.producto.save()
        return super().form_valid(form)