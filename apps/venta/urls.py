from django.conf.urls import url, include

from apps.venta.views import VentaList , VentaCreate

urlpatterns = [
    url(r'^listar$',VentaList.as_view(),name='venta_listar'),
    url(r'^nuevo$',VentaCreate.as_view(),name='venta_crear'),

]