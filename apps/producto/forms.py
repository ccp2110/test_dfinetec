from django import forms

from apps.producto.models import Producto

class ProductoForm(forms.ModelForm):

     class Meta:
         model = Producto

         fields = [
             'nombre',
             'stock',
             'precio_compre',
             'precio_venta',

         ]
         labels ={
             'nombre':'Nombre',
             'stock':'Cantidad en Stock',
             'precio_compre':'Precio de Compra',
             'precio_venta':'Precio de Venta',
         }
         widgets={
             'nombre':forms.TextInput(attrs={'class':'form-control','size':'100'}),
             'stock':forms.NumberInput(attrs={'class': 'form-control'}),
             'precio_compre':forms.NumberInput(attrs={'class': 'form-control'}),
             'precio_venta':forms.NumberInput(attrs={'class': 'form-control'}),
         }