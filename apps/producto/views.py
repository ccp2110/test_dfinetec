from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView , CreateView , UpdateView , DetailView
from apps.producto.models import Producto
from apps.producto.forms import ProductoForm
from django.urls import reverse_lazy

def index(request):
    return HttpResponse("Hola Mundo")

class ProductoList(ListView):
    model = Producto
    template_name = 'producto/producto_list.html'

class ProductoCreate(CreateView):
    model=Producto
    form_class = ProductoForm
    template_name = 'producto/producto_form.html'
    success_url = reverse_lazy('producto_listar')

class ProductoUpdate(UpdateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'producto/producto_form.html'
    success_url = reverse_lazy('producto_listar')