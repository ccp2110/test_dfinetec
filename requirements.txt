Django==2.1.7
django-crispy-forms==1.7.2

Markdown==3.1
psycopg2==2.7.7
psycopg2-binary==2.7.7

html5lib==1.0.1
httplib2==0.9.2


Pygments==2.3.1
pystache==0.5.4
pytz==2018.9
smartypants==2.0.1

python-apt==1.6.2
python-debian==0.1.32


